﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MarsPhotos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string key = "rD80pxIqnUMDEECnaFe0L3jenzz5bVvwWmD2dmIQ";
        string date = "2020-10-12"; //format: YYYY-MM-DD

        List<BitmapImage> pics = new List<BitmapImage>();
        int c = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            pics.Clear();
            c = 0;

            date = dateBox.Text;

            HttpClient client = new HttpClient();

            string data = client.GetStringAsync($"https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date={date}&api_key={key}").Result;

            dynamic jsonData = JObject.Parse(data);

            if(jsonData.photos.Count == 0)
            {
                MessageBox.Show("No photos on that day", "Error");
                return;
            }

            Console.WriteLine(jsonData.photos[0].img_src.Value);

            foreach (var photo in jsonData.photos)
            {
                pics.Add(new BitmapImage(new Uri(photo.img_src.Value)));
            }

            imageBox.Source = pics[0];
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (c >= pics.Count - 1) return;

            c++;
            imageBox.Source = pics[c];
            counterText.Content = $"{c+1}/{pics.Count}";
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (c <= 0) return;

            c--;
            imageBox.Source = pics[c];
            counterText.Content = $"{c + 1}/{pics.Count}";
        }
    }
}
