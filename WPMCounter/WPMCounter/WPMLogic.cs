﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPMCounter
{
    class WPMLogic
    {
        Random r = new Random();
        string[] words = new string[] 
        {
            "hello",
            "goodbye",
            "why",
            "how",
            "understand",
            "analyze",
            "engineer",
            "string",
            "array",
            "using"
        };

        public string GetRandomWord()
        {
            return words[r.Next(words.Length)];
        }

        public int GetCorrectLetters(string currentText, string currentWord)
        {
            int counter;

            for (counter = 0; counter < currentText.Length; counter++)
            {
                if(currentText[counter] != currentWord[counter])
                {
                    break;
                }
            }

            return counter;
        }

    }
}
