﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WPMCounter
{
    public partial class Form1 : Form
    {
        WPMLogic logic = new WPMLogic();
        List<string> currentWords = new List<string>();
        string thisWord;

        TimeSpan time;
        DateTime startTime;

        float WPM;
        int charAmount;

        bool isFirst = true;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            wordsTextBox.Clear();

            //Generate words
            for (int i = 0; i < 10; i++)
            {
                string randomWord = logic.GetRandomWord();
                currentWords.Add(randomWord);
            }
            WriteWords();
            thisWord = currentWords[0];

        }

        private void userTextBox_TextChanged(object sender, EventArgs e)
        {
            if (isFirst)
            {
                backgroundWorker1.RunWorkerAsync();
                startTime = DateTime.Now;
                isFirst = false;
            }

            //Check if word is correct
            if (userTextBox.Text == thisWord)
            {
                userTextBox.Clear();
                currentWords.RemoveAt(0);
                currentWords.Add(logic.GetRandomWord());
                thisWord = currentWords[0];

                WriteWords();
                
            }

            int correctChars = logic.GetCorrectLetters(userTextBox.Text, thisWord);

            #region Text Color
            wordsTextBox.SelectionStart = 0;
            wordsTextBox.SelectionLength = 9999;
            wordsTextBox.SelectionColor = Color.Black;

            wordsTextBox.SelectionStart = 0;
            wordsTextBox.SelectionLength = correctChars;
            wordsTextBox.SelectionColor = Color.Magenta;
            #endregion

            if (userTextBox.TextLength == correctChars)
            {
                charAmount++;
            }
        }

        void WriteWords()
        {
            wordsTextBox.Clear();
            foreach (string word in currentWords)
            {
                wordsTextBox.Text += word + " ";
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (time.TotalSeconds < 30) time = DateTime.Now.Subtract(startTime);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timerText.Text = time.Minutes.ToString("00.##") + ":" + time.Seconds.ToString("00.##") + ":" + (time.Milliseconds/10).ToString("00.##");
            if (time.TotalMinutes == 0) WPMText.Text = "0";
            else WPMText.Text = Math.Round((charAmount / 5.0) / time.TotalMinutes).ToString();

            if (time.TotalSeconds >= 30)
            {
                timer1.Enabled = false;
                MessageBox.Show("FINISH!");
            }
        }
    }
}
