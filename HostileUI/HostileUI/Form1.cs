﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HostileUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("congratulations!");
        }

        void MoveButton(Button btn)
        {
            Random r = new Random();
            int randX = r.Next(this.Size.Width - btn.Width);
            int randY = r.Next(this.Size.Height - btn.Height);

            btn.Location = new Point(randX, randY);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            MoveButton(button1);
            this.Hide();
            new Form1().Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            new Form1().Show();
            new Form1().Show();
            new Form1().Show();
        }
    }
}
