﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Net;

namespace LazyDot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Random r = new Random();
        string[] sentances;
        List<BitmapImage> pics = new List<BitmapImage>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void nextSlide_Click(object sender, RoutedEventArgs e)
        {
            slideText.Text = WebUtility.HtmlDecode(sentances[r.Next(sentances.Length)]);
            
            slideImage.Source = pics[r.Next(pics.Count)];
        }

        private void genButton_Click(object sender, RoutedEventArgs e)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument html = web.Load( urlTextBox.Text );

            //Get text
            string[] paragraphs = html.DocumentNode.SelectNodes("//p").Select(p => p.InnerText).ToArray();
            string text = paragraphs.Aggregate((p1, p2) => p1 + " " + p2);
            sentances = text.Split('.');

            //Get Image
            pics.Clear();
            var imageTags = html.DocumentNode.SelectNodes("//a [@class = 'image']/img");
            foreach (var img in imageTags)
            {
                string picUrl = img.Attributes["src"].Value;
                string[] split = picUrl.Split('/');
                split[5] = split[split.Length - 1] = "";

                char[] result = string.Join("/", split).ToArray();

                result[result.Length - 1] = ' ';

                picUrl = new string(result);

                pics.Add(new BitmapImage(new Uri("https:" + picUrl)));
            }
        }
    }
}
