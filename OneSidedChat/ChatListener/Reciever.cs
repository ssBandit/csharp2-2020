﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace ChatListener
{
    public partial class Reciever : Form
    {
        string ip = "192.168.31.57";
        int port = 666;

        string textBuffer;

        public Reciever()
        {
            InitializeComponent();
        }

        private void Reciever_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            TcpListener listener = new TcpListener(IPAddress.Parse(ip), port);
            listener.Start();

            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream stream = client.GetStream();

                byte[] buffer = new byte[client.ReceiveBufferSize];

                int bytesRead = stream.Read(buffer, 0, client.ReceiveBufferSize);

                string resultText = Encoding.Unicode.GetString(buffer);

                textBuffer += resultText;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (textBuffer != "")
            {
                recieveBox.Text += textBuffer;
                recieveBox.Text += "\n";
                textBuffer = "";
            }
        }
    }
}
