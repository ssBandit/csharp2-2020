﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minesweeper
{
    /*
     * 0-8 num adjacant mines
     * -1 mine
     * -2 pressed button
     */

    public enum NeighboorOperation
    {
        Click,
        Add
    }

    public partial class GameWindow : Form
    {
        int boardSize = 10;
        int cellSize = 30;
        int borderWidth = 1;
        int mineChance = 20;

        Color cellColor = Color.LightGray;
        Color clearColor = Color.LimeGreen;
        Color mineColor = Color.Red;
        Color flagColor = Color.Gold;

        int[,] gameBoard;
        Button[,] buttons;
        int freeSpaces;

        public GameWindow()
        {
            gameBoard = new int[boardSize, boardSize];
            buttons = new Button[boardSize, boardSize];
            freeSpaces = boardSize * boardSize;

            InitializeComponent();
        }

        private void GameWindow_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    CreateButton(x,y);

                    //Place mine with a random chance
                    if(r.Next(0, 100) < mineChance)
                    {
                        gameBoard[x, y] = -1;
                        CheckNeighboors(x,y, NeighboorOperation.Add);
                        freeSpaces--;
                    }
                }
            }
            boardPanel.Size = this.Size = new Size(boardSize * cellSize + boardSize * borderWidth + 40, boardSize * cellSize + boardSize * borderWidth + 50);
        }

        void CreateButton(int x, int y)
        {
            Button cell = new Button();
            cell.Width = cell.Height = cellSize;
            cell.Location = new Point(x * cell.Width + x * borderWidth, y * cell.Height + y * borderWidth);

            cell.FlatStyle = FlatStyle.Flat;
            cell.FlatAppearance.BorderSize = 0;
            cell.TabStop = false;
            cell.BackColor = cellColor;

            cell.Click += (snd, ev) => Cell_Click(snd, ev, x, y);
            cell.MouseUp += Cell_MouseUp;

            boardPanel.Controls.Add(cell);

            buttons[x, y] = cell;
        }

        private void Cell_MouseUp(object sender, MouseEventArgs e)
        {
            Button buttonPressed = (sender as Button);
            if (e.Button == MouseButtons.Right)
            {
                if(buttonPressed.BackColor == cellColor)
                {
                    buttonPressed.BackColor = flagColor;
                }
                else if(buttonPressed.BackColor == flagColor)
                {
                    buttonPressed.BackColor = cellColor;
                }
            }
        }

        private void Cell_Click(object sender, EventArgs e, int x, int y)
        {
            if (gameBoard[x, y] == 0)
            {
                (sender as Button).BackColor = clearColor;
                gameBoard[x, y] = -2;
                CheckNeighboors(x,y, NeighboorOperation.Click);
                freeSpaces--;
            }
            else if(gameBoard[x,y] > 0)
            {
                (sender as Button).Text = gameBoard[x, y].ToString();
                (sender as Button).BackColor = clearColor;
                gameBoard[x, y] = -2;
                freeSpaces--;
            }
            else if (gameBoard[x, y] == -1)
            {
                for (int xp = 0; xp < boardSize; xp++)
                {
                    for (int yp = 0; yp < boardSize; yp++)
                    {
                        if (gameBoard[xp, yp] == -1)
                        {
                            buttons[xp, yp].BackColor = mineColor;
                        }
                    }
                }

                MessageBox.Show("You exploded to gibs", "BOOOOOM");
                Application.Restart();
            }

            if(freeSpaces == 0)
            {
                MessageBox.Show("You did it, you cleared all the mines", "Yes!");
                Application.Exit();
            }
        }

        void CheckNeighboors(int x, int y, NeighboorOperation operation)
        {
            //Horizontal Check
            if(x > 0 && gameBoard[x - 1, y] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x - 1, y].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x - 1, y]++;
                }
            }
            if(x < boardSize - 1 && gameBoard[x + 1, y] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x + 1, y].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x + 1, y]++;
                }
            }

            //Verical check
            if (y > 0 && gameBoard[x, y - 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x, y - 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x, y - 1]++;
                }
            }
            if (y < boardSize - 1 && gameBoard[x, y + 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x, y + 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x, y + 1]++;
                }
            }

            //Diagonal check

            if (x > 0 && y > 0 && gameBoard[x - 1, y - 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x - 1, y - 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x - 1, y - 1]++;
                }
            }
            if (x < boardSize - 1 && y < boardSize - 1 && gameBoard[x + 1, y + 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x + 1, y + 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x + 1, y + 1]++;
                }
            }

            if (x > 0 && y < boardSize - 1 && gameBoard[x - 1, y + 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x - 1, y + 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x - 1, y + 1]++;
                }
            }
            if (x < boardSize - 1 && y > 0 && gameBoard[x + 1, y - 1] >= 0)
            {
                if (operation == NeighboorOperation.Click)
                {
                    buttons[x + 1, y - 1].PerformClick();
                }
                else if (operation == NeighboorOperation.Add)
                {
                    gameBoard[x + 1, y - 1]++;
                }
            }

        }
    }
}
