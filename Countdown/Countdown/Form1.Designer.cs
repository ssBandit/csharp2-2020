﻿namespace Countdown
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerText = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.urlText = new System.Windows.Forms.TextBox();
            this.timeZonePicker = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // timerText
            // 
            this.timerText.AutoSize = true;
            this.timerText.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timerText.Location = new System.Drawing.Point(132, 41);
            this.timerText.Name = "timerText";
            this.timerText.Size = new System.Drawing.Size(533, 135);
            this.timerText.TabIndex = 0;
            this.timerText.Text = "00:00:00";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(622, 344);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(81, 70);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // urlText
            // 
            this.urlText.Location = new System.Drawing.Point(155, 392);
            this.urlText.Name = "urlText";
            this.urlText.Size = new System.Drawing.Size(200, 22);
            this.urlText.TabIndex = 3;
            // 
            // timeZonePicker
            // 
            this.timeZonePicker.FormattingEnabled = true;
            this.timeZonePicker.Items.AddRange(new object[] {
            "NONE",
            "UTC",
            "CET",
            "CEST",
            "EST",
            "CET",
            "PST"});
            this.timeZonePicker.Location = new System.Drawing.Point(403, 344);
            this.timeZonePicker.Name = "timeZonePicker";
            this.timeZonePicker.Size = new System.Drawing.Size(121, 24);
            this.timeZonePicker.TabIndex = 4;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(155, 346);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(103, 203);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(600, 54);
            this.progressBar1.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.timeZonePicker);
            this.Controls.Add(this.urlText);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.timerText);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label timerText;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox urlText;
        private System.Windows.Forms.ComboBox timeZonePicker;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

