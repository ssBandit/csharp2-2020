﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Countdown
{
    public partial class Form1 : Form
    {
        DateTime setTime;
        TimeSpan timeRemaining;

        double totalSecondsRemaining;

        Dictionary<string, int> timezones = new Dictionary<string, int>
        {
            {"NONE", 0},
            {"CEST", -1},
            {"CET", -2},
            {"UTC", -3},
            {"EST", -8}
        };

        public Form1()
        {
            InitializeComponent();
            timeZonePicker.SelectedIndex = 0;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            try
            {
                setTime = dateTimePicker1.Value;

                setTime = setTime.AddHours(-timezones[timeZonePicker.Text]);
                timeRemaining = setTime.Subtract(DateTime.Now);

                totalSecondsRemaining = timeRemaining.TotalSeconds;

                if (timeRemaining.TotalSeconds <= 0)
                {
                    throw new ArithmeticException();
                }

                timer1.Start();
            }
            catch (ArithmeticException)
            {
                MessageBox.Show("time has passed :(", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch
            {
                MessageBox.Show("i don't even know what the fuck happened, but it's bad", "????", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timeRemaining = setTime.Subtract(DateTime.Now);

            timerText.Text = $"{timeRemaining.Days}d {timeRemaining.Hours.ToString("D2")}:{timeRemaining.Minutes.ToString("D2")}:{timeRemaining.Seconds.ToString("D2")}";

            progressBar1.Value = (int)(100 - timeRemaining.TotalSeconds / totalSecondsRemaining * 100);

            if(timeRemaining.TotalSeconds <= 0)
            {
                progressBar1.Value = progressBar1.Maximum;
                timer1.Stop();
                if (urlText.Text != string.Empty)
                {
                    Process.Start(urlText.Text);
                }
            }
        }
    }
}
