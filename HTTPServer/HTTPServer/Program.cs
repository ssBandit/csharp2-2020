﻿using System;
using System.Net;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Web;

namespace HTTPServer
{
    class Program
    {

        static List<string> comments = new List<string>();

        static void Main(string[] args)
        {
            Console.WriteLine("LAUNCHED!");

            HttpListener server = new HttpListener();

            server.Prefixes.Add("http://*:8081/");

            server.Start();

            Console.WriteLine("Listening");

            while (true)
            {
                HttpListenerContext context = server.GetContext();
                HttpListenerRequest request = context.Request;

                Console.WriteLine(request.RemoteEndPoint);

                if(request.HttpMethod == "POST")
                {
                    Stream inputStream = request.InputStream;
                    StreamReader reader = new StreamReader(inputStream, request.ContentEncoding);

                    string message = reader.ReadToEnd();
                    message = HttpUtility.UrlDecode(message.Replace("comment=", ""));
                    Console.WriteLine(message);
                    comments.Add(message);
                }

                string response = File.ReadAllText(@"..\..\..\home.html") + request.RemoteEndPoint;

                foreach (string comment in comments)
                {
                    response += $"<li>{comment}</li>";
                }

                byte[] buffer = Encoding.UTF8.GetBytes(response);

                context.Response.ContentLength64 = buffer.Length;
                Stream output = context.Response.OutputStream;

                output.Write(buffer, 0, buffer.Length);

                output.Close();
            }
        }
    }
}
