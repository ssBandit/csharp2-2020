﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContentScale
{
    public partial class Form1 : Form
    {
        Scale scale = new Scale();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int rowsToRemove = int.Parse(textBox1.Text);

            for (int i = 0; i < rowsToRemove; i++)
            {
                Bitmap pic = (Bitmap)pictureBox1.Image;

                scale.CalculateEnergy(pic);
                scale.FindPath();
                scale.Resize();

                pictureBox1.Image = scale.resizedimage;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            pictureBox1.Image = new Bitmap(openFileDialog1.FileName);
        }
    }
}
