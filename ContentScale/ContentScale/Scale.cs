﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastBitmapLib;

namespace ContentScale
{
    class Scale
    {
        public Bitmap original;
        public Bitmap energyimage;
        public Bitmap pathimage;
        public Bitmap resizedimage;

        List<Coordinate> path = new List<Coordinate>();

        Random r = new Random();

        public void CalculateEnergy(Bitmap image)
        {
            original = new Bitmap(image);
            energyimage = new Bitmap(image);

            FastBitmap orig = new FastBitmap(original);
            FastBitmap nrg = new FastBitmap(energyimage);

            orig.Lock();
            nrg.Lock();
       
            for (int x = 0; x < energyimage.Width - 1; x++)                      
            {                                                                    
                for (int y = 0; y < energyimage.Height; y++)                     
                {
                    Color result = ColorDistance(orig.GetPixel(x, y), orig.GetPixel(x + 1, y));

                    nrg.SetPixel(x, y, result);
                }
            }

            orig.Unlock();
            nrg.Unlock();
        }

        public void FindPath()
        {
            path.Clear();
            pathimage = new Bitmap(energyimage);

            FastBitmap pathimg = new FastBitmap(pathimage);
            FastBitmap nrg = new FastBitmap(energyimage);

            pathimg.Lock();
            nrg.Lock();

            path.Add(new Coordinate(r.Next(0, energyimage.Width), 0));

            for (int y = 1; y < pathimage.Height; y++)
            {
                int below = nrg.GetPixel(path[y-1].x, y).R;
                int left = nrg.GetPixel(path[y-1].x - (path[y-1].x == 0 ? 0 : 1), y).R;
                int right = nrg.GetPixel(path[y-1].x + (path[y - 1].x == pathimage.Width-1 ? 0 : 1), y).R;

                int smallest = Math.Min(Math.Min(left, right), below);

                if (smallest == below) path.Add(new Coordinate(path[y - 1].x, y));
                else if (smallest == right) path.Add(new Coordinate(path[y - 1].x - (path[y - 1].x == 0 ? 0 : 1), y));
                else path.Add(new Coordinate(path[y - 1].x + (path[y - 1].x == pathimage.Width - 1 ? 0 : 1), y));
            }

            foreach (var coord in path)
            {
                pathimg.SetPixel(coord.x, coord.y, Color.Red);
            }

            pathimg.Unlock();
            nrg.Unlock();
        }

        public void Resize()
        {
            resizedimage = new Bitmap(original);

            FastBitmap orig = new FastBitmap(original);
            FastBitmap resized = new FastBitmap(resizedimage);

            orig.Lock();
            resized.Lock();

            foreach (var coord in path)
            {
                for (int x = coord.x; x < original.Width - 1; x++)
                {
                    resized.SetPixel(x, coord.y, orig.GetPixel(x + 1, coord.y));
                }
            }

            orig.Unlock();
            resized.Unlock();
            
        }

        Color ColorDistance(Color pix1, Color pix2)
        {
            int R1 = pix1.R;
            int G1 = pix1.G;
            int B1 = pix1.B;

            int R2 = pix2.R;
            int G2 = pix2.G;
            int B2 = pix2.B;

            int R = Math.Abs(R1 - R2);
            int G = Math.Abs(G1 - G2);
            int B = Math.Abs(B1 - B2);

            int color = R + G + B;

            color = Math.Max(Math.Min(color, 255), 0);

            return Color.FromArgb(color, color, color);
        }

    }

    struct Coordinate
    {
        public int x;
        public int y;

        public Coordinate(int newX, int newY)
        {
            x = newX;
            y = newY;
        }
    }
}
