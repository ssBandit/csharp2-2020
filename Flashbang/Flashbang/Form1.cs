﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Threading;

namespace Flashbang
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Flashbang.WindowsHooks.SetBrightness(100);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Show();
            Update();
            timer1.Enabled = false;
            new SoundPlayer("sas_ct_flashbang03.wav").Play();
            Thread.Sleep(1000);
            //Flashbang.WindowsHooks.BlowTheSpeakersUp();
            //Flashbang.WindowsHooks.SetBrightness(255);

            BrightScreen bs = new BrightScreen();
            bs.Show();

            Console.Beep(2800, 3000);
        }
    }
}
