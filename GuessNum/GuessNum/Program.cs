﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        Random r = new Random();
        int rand = r.Next(11);
        Console.WriteLine(rand);

        for (int i = 0; i < 4; i++)
        {
            Console.WriteLine("Guess a number");
            var guess = int.Parse(Console.ReadLine());
            
            if (guess == rand)
            {
                //comment
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("You did it!!!");
                Console.Beep(800, 5000);
                break;
            }
            else if (guess < rand)
            {
                Console.WriteLine("bigger");
            }
            else if (guess > rand)
            {
                Console.WriteLine("smaller");
            }
        }

        Console.ReadLine();
    }
}
