﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace HashDestroyer
{
    class Program
    {

        //Буквы из которых выбирать
        static string letters = "abcdefghijklmnopqrstuvwxyz";
        //Оригинальный хешь который хотим вскрыть
        static string hash = "4019f26e6a78551fff73b65ef5bdd922da5f0490d25014764825bfa4a0393737";

        static void Main(string[] args)
        {
            //Запуск треда с функцией вывода в консоль
            Thread logger = new Thread(new ThreadStart(Log));
            logger.Start();

            //Тестовый хеш
            string hash = GetHash("hello");
            Console.WriteLine(hash);

            //Создать и запустить секундомер
            Stopwatch time = new Stopwatch();
            time.Start();

            //Взломать хешь подбором из списка паролей
            //BreakHash("f223fdb52a594efbc9dd3d10993f504d6c240b092ce9b5a96bfbad4507214dd7");

            //Взломать хешь перебором всех букв
            BruteForce();

            //Остановить секундомер
            time.Stop();
            //Вывести как быстро функция сработала
            Console.WriteLine("Completed in " + time.Elapsed);
            Console.ReadKey();
        }

        //Взлом хеша методом перебора из списка паролей
        static void BreakHash(string originalHash)
        {
            //Открыть список паролей и загрузить его строчки в массив
            string[] lines = File.ReadAllLines("passwords.txt");

            //Пройтись по каждой строчке, хешировать её, и если совпадает с оригиналом то остановить
            foreach (string pass in lines)
            {
                string hashed = GetHash(pass);

                Console.WriteLine(hashed);

                if(hashed == originalHash)
                {
                    Console.WriteLine("FOUND!!!");
                    Console.WriteLine(pass + " : " + hashed + " == " + originalHash);
                    return;
                }
            }
            Console.WriteLine("Couldn't find the password :(");
        }

        //Вычесление хеша
        static string GetHash(string text)
        {
            //Создать генератор хешов
            SHA256 hash = SHA256.Create();

            //Конвертировать текст в байты используя UTF8 кодировку
            byte[] textBytes = System.Text.Encoding.UTF8.GetBytes(text);

            //Вычеслить хеш
            byte[] hashBytes = hash.ComputeHash(textBytes);

            string result = "";

            //Пройтись по каждому байту, конвертировать его в 16ти значное число и добавить к результату
            foreach (byte b in hashBytes)
            {
                result += b.ToString("x2");
            }

            return result;
        }

        //Запустить брут-форс рекурсию на 6 букв
        static void BruteForce()
        {
            for (int i = 0; i < 6; i++)
            {
                BruteRecurse(i);
            }
        }

        static bool stopRecurse = false;
        static void BruteRecurse(int max, int depth = 0, string result = "")
        {
            //Пройтись по каждой букве алфавита и пока не дошол до дна рекурсии вызвать еще раз эту-же функцию
            foreach (char c in letters)
            {
                if (depth < max)
                {
                    BruteRecurse(max, depth + 1, result + c);
                    if (stopRecurse) return;
                }
                else
                {
                    //Console.WriteLine(result + c);
                    globalResult = result + c;
                    //Если хеш равняется оригиналу, значит мы его нашли
                    if (GetHash(result + c) == hash)
                    {
                        Console.WriteLine("FOUND");
                        Console.WriteLine(result + c);
                        stopRecurse = true;
                    }
                }
            }
        }

        static string globalResult = "";
        static void Log()
        {
            while(!stopRecurse)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(globalResult);
            }
        }
    }
}
